import type { NextPage } from "next";
import { QuestionList } from "../screens/QuestionsList/QuestionList";
import React from "react";

const Home: NextPage = () => {
  return <QuestionList />;
};

export default Home;
