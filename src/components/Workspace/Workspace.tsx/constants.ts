import { NavbarLink } from "../../Navbar/types";

export const NAVBAR_LINKS: NavbarLink[] = [
  { title: "Browse", icon: "/assets/bars.svg", href: "#" },
  { title: "Add new questions", icon: "/assets/plus.svg", href: "#" },
  { title: "Api", icon: "/assets/setting.svg", href: "#" },
  { title: "Discuss", icon: "/assets/comments.svg", href: "#" },
  { title: "Login", icon: "/assets/signin.svg", href: "#" },
];
