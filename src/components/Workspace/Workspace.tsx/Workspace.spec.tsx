import { render, screen } from "@testing-library/react";

import { NAVBAR_LINKS } from "./constants";
import React from "react";
import { Workspace } from "./Workspace";

describe("Workspace", () => {
  it("renders navbar and children", () => {
    render(
      <Workspace>
        <h1>test</h1>
      </Workspace>
    );

    NAVBAR_LINKS.forEach(({ title, href }) => {
      expect(screen.getByRole("link", { name: title })).toHaveAttribute(
        "href",
        href
      );
    });
    expect(screen.getByText("test")).toBeInTheDocument();
  });

  it("renders spinner while loading", () => {
    render(
      <Workspace isLoading={true}>
        <h1>test</h1>
      </Workspace>
    );

    expect(screen.getByRole("progressbar")).toBeVisible();
    expect(screen.queryByText("test")).not.toBeInTheDocument()
  });
});
