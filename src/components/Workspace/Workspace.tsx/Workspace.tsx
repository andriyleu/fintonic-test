import { NAVBAR_LINKS } from "./constants";
import { Navbar } from "../../Navbar/Navbar";
import React from "react";

type WorkspaceProps = {
  children: JSX.Element;
  isLoading?: boolean;
};

export const Workspace = ({ children, isLoading = false }: WorkspaceProps) => {
  return (
    <div className="min-h-screen bg-gray-800">
      <header className="sticky top-0 z-50">
        <Navbar links={NAVBAR_LINKS} />
      </header>
      <main className="flex flex-col justify-center items-center">
        {isLoading ? (
          <div
            role="progressbar"
            className="
            animate-spin
            rounded-full
            h-16
            w-16
            border-t-2 border-b-2 border-green-500
            absolute top-1/2 left-1/2
          "
          ></div>
        ) : (
          children
        )}
      </main>
    </div>
  );
};
