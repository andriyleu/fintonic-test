import { render, screen } from "@testing-library/react";

import { Pagination } from "./Pagination";
import React from "react";
import userEvent from "@testing-library/user-event";

const PAGE_COUNT = 5;

describe("Pagination", () => {
  it("renders pageCount number of buttons and two extra button for next and previous", () => {
    render(
      <Pagination
        onPreviousClick={() => undefined}
        canPrevious={false}
        onNextClick={() => undefined}
        canNext={false}
        currentPage={0}
        onPageIndexChanged={() => undefined}
        pageCount={PAGE_COUNT}
      ></Pagination>
    );
    Array.from(Array(PAGE_COUNT).keys()).forEach((pageIndex) => {
      expect(
        screen.getByRole("button", { name: String(pageIndex + 1) })
      ).toBeVisible();
    });
    expect(screen.getByRole("button", { name: "<" })).toBeVisible();
    expect(screen.getByRole("button", { name: ">" })).toBeVisible();
  });

  it("calls callback when buttons clicked", () => {
    const onPreviousClickMock = jest.fn();
    const onPageIndexChanged = jest.fn();
    const onNextClick = jest.fn();

    render(
      <Pagination
        onPreviousClick={onPreviousClickMock}
        canPrevious={true}
        onNextClick={onNextClick}
        canNext={true}
        currentPage={1}
        onPageIndexChanged={onPageIndexChanged}
        pageCount={PAGE_COUNT}
      ></Pagination>
    );

    userEvent.click(screen.getByRole("button", { name: "2" }));
    expect(onPageIndexChanged).toHaveBeenCalledWith(1);
    userEvent.click(screen.getByRole("button", { name: "<" }));
    expect(onPreviousClickMock).toHaveBeenCalled();
    userEvent.click(screen.getByRole("button", { name: ">" }));
    expect(onNextClick).toHaveBeenCalled();
  });

  it("buttons are disabled when props passed", () => {
    const onPreviousClickMock = jest.fn();
    const onNextClick = jest.fn();

    render(
      <Pagination
        onPreviousClick={onPreviousClickMock}
        canPrevious={false}
        onNextClick={onNextClick}
        canNext={false}
        currentPage={1}
        onPageIndexChanged={() => undefined}
        pageCount={PAGE_COUNT}
      ></Pagination>
    );

    userEvent.click(screen.getByRole("button", { name: "<" }));
    expect(onPreviousClickMock).not.toHaveBeenCalled();
    userEvent.click(screen.getByRole("button", { name: ">" }));
    expect(onNextClick).not.toHaveBeenCalled();
  });
});
