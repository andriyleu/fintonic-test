type PaginationProps = {
  onPreviousClick: () => void;
  canPrevious: boolean;
  onNextClick: () => void;
  canNext: boolean;
  currentPage: number;
  onPageIndexChanged: (newPage: number) => void;
  pageCount: number;
};

export const Pagination = ({
  onPreviousClick,
  onNextClick,
  canNext,
  canPrevious,
  currentPage,
  onPageIndexChanged,
  pageCount,
}: PaginationProps) => {
  return (
    <nav
      className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px"
      aria-label="Pagination"
    >
      <button
        disabled={!canPrevious}
        onClick={onPreviousClick}
        className="relative bg-green-800 inline-flex items-center px-4 py-4 rounded-l-md  text-sm font-medium text-white hover:bg-green-500"
      >
        {"<"}
      </button>

      {Array.from(Array(pageCount).keys()).map((pageIndex) => {
        return (
          <button
            key={pageIndex}
            aria-current={pageIndex === currentPage}
            className={`z-10 text-white relative inline-flex items-center px-4 py-4 text-sm font-medium hover:bg-green-500 ${
              pageIndex === currentPage ? "bg-green-500" : "bg-green-800"
            }`}
            onClick={() => onPageIndexChanged(pageIndex)}
          >
            {pageIndex + 1}
          </button>
        );
      })}
      <button
        disabled={!canNext}
        onClick={onNextClick}
        className="relative inline-flex items-center px-4 py-4 rounded-r-md bg-green-800 text-sm font-medium text-white hover:bg-green-500"
      >
        {">"}
      </button>
    </nav>
  );
};
