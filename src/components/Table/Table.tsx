import { useGlobalFilter, usePagination, useTable } from "react-table";

import { Pagination } from "components/Pagination/Pagination";

type TableProps = {
  data: any;
  columns: any;
  title: string;
};

export const Table = ({ columns, data, title }: TableProps) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    canPreviousPage,
    canNextPage,
    pageCount,
    page,
    gotoPage,
    nextPage,
    setGlobalFilter,
    previousPage,
    state: { pageIndex, globalFilter },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0, pageSize: 10, globalFilter: "" },
    },
    useGlobalFilter,
    usePagination
  );

  return (
    <>
      <div>
        <div className="flex justify-between mt-8 mb-4">
          <h2 className="text-4xl	text-white">{title}</h2>
          <input
            type="text"
            aria-label="Search"
            value={globalFilter}
            onChange={(e) => setGlobalFilter(e.target.value)}
            placeholder="Search..."
          />
        </div>
        <table className="table-auto text-white mb-8" {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr
                className="bg-gray-800"
                {...headerGroup.getHeaderGroupProps()}
                key={headerGroup.id}
              >
                {headerGroup.headers.map((column) => (
                  <th
                    className="border-collapse border border-gray-600 min-w-sm p-2"
                    {...column.getHeaderProps()}
                    key={column.id}
                  >
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row);
              return (
                <tr className="bg-gray-900" {...row.getRowProps()} key={row.id}>
                  {row.cells.map((cell) => (
                    <td
                      className="border-collapse border border-gray-600 max-w-sm p-2"
                      {...cell.getCellProps()}
                      key={cell.value}
                    >
                      {cell.render("Cell")}
                    </td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
        {page.length === 0 && <span>No results found</span>}
      </div>
      <Pagination
        onPreviousClick={previousPage}
        canPrevious={canPreviousPage}
        onNextClick={nextPage}
        canNext={canNextPage}
        currentPage={pageIndex}
        onPageIndexChanged={(pageIndex) => gotoPage(pageIndex)}
        pageCount={pageCount}
      ></Pagination>
    </>
  );
};
