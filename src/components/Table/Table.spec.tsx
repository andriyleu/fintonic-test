import { render, screen } from "@testing-library/react";

import React from "react";
import { Table } from "./Table";
import userEvent from "@testing-library/user-event";

const COLUMNS = [
  {
    Header: "Id",
    accessor: ({ id }: DataExample) => id,
  },
  {
    Header: "Name",
    accessor: ({ name }: DataExample) => name,
  },
];

type DataExample = {
  id: number;
  name: string;
};

const DATA: DataExample[] = Array.from(Array(20).keys()).map((pageIndex) => ({
  id: pageIndex,
  name: `Todo number: ${pageIndex}`,
}));

const FIRST_PAGE = DATA.slice(0, 10);

describe("Table", () => {
  it("renders title, columns and rows from first page", () => {
    render(<Table columns={COLUMNS} data={DATA} title="test" />);

    expect(screen.getByText("test")).toBeVisible();
    expect(screen.getByText("Id")).toBeVisible();
    expect(screen.getByText("Name")).toBeVisible();

    FIRST_PAGE.forEach(({ id, name }) => {
      expect(screen.getByRole("cell", { name: String(id) })).toBeVisible();
      expect(screen.getByRole("cell", { name: name })).toBeVisible();
    });
  });

  it("displays empty label when no rows", () => {
    render(<Table columns={COLUMNS} data={[]} title="test" />);

    expect(screen.getByText("No results found")).toBeVisible();
  });

  it("first page is selected by default", () => {
    render(<Table columns={COLUMNS} data={DATA} title="test" />);

    expect(screen.getByRole("button", { current: true })).toHaveTextContent(
      "1"
    );
  });

  it("allows to paginate", () => {
    render(<Table columns={COLUMNS} data={DATA} title="test" />);

    // assert previous click being disabled on 1st page
    expect(screen.getByRole("button", { name: "<" })).toBeDisabled();

    // click on next page and check if 2nd page is selected
    userEvent.click(screen.getByRole("button", { name: ">" }));
    expect(screen.getByRole("button", { current: true })).toHaveTextContent(
      "2"
    );

    // assert next click being disabled on 2nd page
    expect(screen.getByRole("button", { name: ">" })).toBeDisabled();

    // click on previous page and check if 1st page is selected
    userEvent.click(screen.getByRole("button", { name: "<" }));
    expect(screen.getByRole("button", { current: true })).toHaveTextContent(
      "1"
    );

    // click on 2nd page and check if 2nd page is selected
    userEvent.click(screen.getByRole("button", { name: "2" }));
    expect(screen.getByRole("button", { current: true })).toHaveTextContent(
      "2"
    );
  });

  it("allows global filtering", () => {
    render(<Table columns={COLUMNS} data={DATA} title="test" />);

    userEvent.type(screen.getByLabelText("Search"), "Todo number: 19");
    expect(screen.getByRole("cell", { name: "Todo number: 19" })).toBeVisible();
    expect(screen.getAllByRole("row")).toHaveLength(2); // header is a row also

    // add something random so results not found is thrown
    userEvent.type(screen.getByLabelText("Search"), "test");
    expect(screen.getByText("No results found")).toBeVisible();
  });
});
