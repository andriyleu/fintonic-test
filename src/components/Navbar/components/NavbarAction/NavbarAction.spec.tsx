import { render, screen } from "@testing-library/react";

import { NavbarAction } from "./NavbarAction";
import { NavbarLink } from "components/Navbar/types";
import React from "react";

const TEST_ACTION: NavbarLink = {
  title: "test",
  icon: "/assets/bars.svg",
  href: "/test",
};

describe("NavbarAction", () => {
  it("renders title and icon", () => {
    render(<NavbarAction link={TEST_ACTION} />);

    expect(screen.getByText(TEST_ACTION.title)).toBeInTheDocument();
    expect(screen.getByAltText(TEST_ACTION.title)).toBeInTheDocument();
  });

  it("has expected link", () => {
    render(<NavbarAction link={TEST_ACTION} />);

    expect(
      screen.getByRole("link", { name: TEST_ACTION.title })
    ).toHaveAttribute("href", "/test");
  });
});
