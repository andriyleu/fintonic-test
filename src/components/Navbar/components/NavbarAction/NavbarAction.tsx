import Image from "next/image";
import { NavbarLink } from "../../types";

type NavbarActionProps = {
  link: NavbarLink;
};

export const NavbarAction = ({
  link: { title, icon, href },
}: NavbarActionProps) => {
  return (
    <a
      href={href}
      aria-label={title}
      className="flex align-center bg-gray-800 ml-4 p-2 text-white uppercase"
    >
      <Image alt={title} src={icon} height={16} width={16} />
      <span className="ml-2">{title}</span>
    </a>
  );
};
