import { NavbarAction } from "./components/NavbarAction/NavbarAction";
import { NavbarLink } from "./types";
import React from "react";

export type NavbarProps = {
  links: NavbarLink[];
};

export const Navbar = ({ links }: NavbarProps) => {
  return (
    <nav className="flex items-center justify-end flex-wrap bg-gray-900 p-4">
      <div className="flex text-sm">
        {links.map((link, index) => (
          <NavbarAction key={index} link={link} />
        ))}
      </div>
    </nav>
  );
};
