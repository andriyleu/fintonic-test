export type NavbarLink = {
  title: string;
  icon: string;
  href: string;
};
