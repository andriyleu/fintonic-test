import { render, screen } from "@testing-library/react";

import { Navbar } from "./Navbar";
import { NavbarLink } from "components/Navbar/types";
import React from "react";

const TEST_ACTIONS: NavbarLink[] = [
  {
    title: "test1",
    icon: "/assets/bars.svg",
    href: "/test1",
  },
  {
    title: "test2",
    icon: "/assets/comments.svg",
    href: "/test2",
  },
  {
    title: "test3",
    icon: "/assets/signin.svg",
    href: "/test3",
  },
];

describe("Navbar", () => {
  it("renders all links", () => {
    render(<Navbar links={TEST_ACTIONS} />);

    TEST_ACTIONS.forEach((action) => {
      expect(
        screen.getByRole("link", { name: action.title })
      ).toBeInTheDocument();
    });
  });
});
