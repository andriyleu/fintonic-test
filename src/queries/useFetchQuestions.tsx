import { Question, Status } from "models/question";

import { API_ENDPOINT } from "constants/constants";
import { uid } from "uid";
import { useQuery } from "react-query";

const MAX_ELEMENTS = 50; // maximum elements allowed to query according to api doc

export const useFetchQuestions = () => {
  const request = useQuery<Status<Question>>("questions", () =>
    fetch(`${API_ENDPOINT}?amount=${MAX_ELEMENTS}`).then(async (res) => {
      const response = await res.json();

      const mappedQuestions = response.results.map((question: Question) => ({
        ...question,
        id: uid(4),
        created_by: "Andriy",
      }));

      return {
        ...response,
        results: mappedQuestions,
      };
    })
  );

  return {
    ...request,
    questions: request.data?.results ?? [],
  };
};
