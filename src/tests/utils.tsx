import { QueryClient, QueryClientProvider } from "react-query";
import { ReactElement, ReactNode } from "react";

import { render as originalRenderer } from "@testing-library/react";

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: 0,
      useErrorBoundary: false,
      refetchOnWindowFocus: false,
    },
    mutations: {
      useErrorBoundary: false,
    },
  },
});

export const customRender = (
  component: ReactElement,
  { ...renderOptions }: any = {}
) => {
  const Wrapper = ({ children }: { children: ReactNode }) => (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
  );
  return originalRenderer(component, { wrapper: Wrapper, ...renderOptions });
};

export * from "@testing-library/react";
export { customRender as render };
