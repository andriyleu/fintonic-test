import {
  queryByRole,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from "tests/utils";

import { API_ENDPOINT } from "constants/constants";
import { QUESTIONS } from "fixtures/questions";
import { QUESTION_TABLE_COLUMNS } from "./constants";
import { QuestionList } from "./QuestionList";
import React from "react";
import fetchMock from "fetch-mock";
import userEvent from "@testing-library/user-event";

const FIRST_QUESTIONS_PAGE = QUESTIONS.results.slice(0, 10);
const SECOND_QUESTIONS_PAGE = QUESTIONS.results.slice(10, 20);
const FIRST_QUESTION = QUESTIONS.results[0];
const getProgressBar = () => screen.getByRole("progressbar");

describe("QuestionList", () => {
  it("renders title", async () => {
    fetchMock.get(`begin:${API_ENDPOINT}`, QUESTIONS);

    render(<QuestionList />);

    await waitForElementToBeRemoved(getProgressBar);

    expect(screen.getByText("Browse Question"));
  });

  it("renders question's columns and rows", async () => {
    fetchMock.get(`begin:${API_ENDPOINT}`, QUESTIONS);

    render(<QuestionList />);

    await waitForElementToBeRemoved(getProgressBar);

    QUESTION_TABLE_COLUMNS.forEach((column) => {
      expect(screen.getByText(column.Header)).toBeVisible;
    });

    FIRST_QUESTIONS_PAGE.forEach(({ question }) => {
      expect(screen.getByText(question)).toBeVisible;
    });
  });

  it("allows to paginate", async () => {
    fetchMock.get(`begin:${API_ENDPOINT}`, QUESTIONS);

    render(<QuestionList />);

    await waitForElementToBeRemoved(getProgressBar);

    userEvent.click(screen.getByRole("button", { name: ">" }));

    SECOND_QUESTIONS_PAGE.forEach(({ question }) => {
      expect(screen.getByText(question)).toBeVisible;
    });
  });

  it("allows to filter", async () => {
    fetchMock.get(`begin:${API_ENDPOINT}`, QUESTIONS);

    render(<QuestionList />);

    await waitForElementToBeRemoved(getProgressBar);

    // click on next page and check if 2nd page is selected
    userEvent.type(screen.getByLabelText("Search"), FIRST_QUESTION.question);

    expect(screen.getByRole("cell", { name: FIRST_QUESTION.question }));
  });
});
