import React, { useMemo } from "react";

import { QUESTION_TABLE_COLUMNS } from "./constants";
import { Table } from "components/Table/Table";
import { Workspace } from "../../components/Workspace/Workspace.tsx/Workspace";
import { useFetchQuestions } from "queries/useFetchQuestions";

export const QuestionList = () => {
  const { questions, isLoading } = useFetchQuestions();
  const columns = useMemo(() => QUESTION_TABLE_COLUMNS, []);

  return (
    <Workspace isLoading={isLoading}>
      <Table columns={columns} data={questions} title="Browse Question"></Table>
    </Workspace>
  );
};
