import { Question } from "models/question";

export const QUESTION_TABLE_COLUMNS = [
  {
    Header: "ID",
    accessor: ({ id }: Question) => id,
  },
  {
    Header: "Category",
    accessor: ({ category }: Question) => category,
  },
  {
    Header: "Type",
    accessor: ({ type }: Question) => type,
  },
  {
    Header: "Difficulty",
    accessor: ({ difficulty }: Question) => difficulty,
  },
  {
    Header: "Question",
    accessor: ({ question }: Question) => question,
  },
  {
    Header: "Created By",
    accessor: ({ created_by }: Question) => created_by,
    Cell: ({ cell: { value: created_by } }: { cell: { value: string } }) => (
      <span className="text-green-400">{created_by}</span>
    ),
  },
];
