export const QUESTIONS = {
  response_code: 0,
  results: [
    {
      category: "History",
      type: "multiple",
      difficulty: "easy",
      question:
        "Which one of these was not a beach landing site in the Invasion of Normandy?",
      correct_answer: "Silver",
      incorrect_answers: ["Gold", "Juno", "Sword"],
    },
    {
      category: "Entertainment: Music",
      type: "multiple",
      difficulty: "medium",
      question:
        "Who had hits in the 70s with the songs &quot;Lonely Boy&quot; and &quot;Never Let Her Slip Away&quot;?",
      correct_answer: "Andrew Gold",
      incorrect_answers: ["Elton John", "Leo Sayer", "Barry White "],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "medium",
      question: "What was the code name for the &quot;Nintendo Gamecube&quot;?",
      correct_answer: "Dolphin",
      incorrect_answers: ["Nitro", "Revolution", "Atlantis"],
    },
    {
      category: "Science & Nature",
      type: "multiple",
      difficulty: "easy",
      question: "Which type of rock is created by intense heat AND pressure?",
      correct_answer: "Metamorphic",
      incorrect_answers: ["Sedimentary", "Igneous", "Diamond"],
    },
    {
      category: "Animals",
      type: "boolean",
      difficulty: "easy",
      question: "A slug&rsquo;s blood is green.",
      correct_answer: "True",
      incorrect_answers: ["False"],
    },
    {
      category: "Entertainment: Television",
      type: "multiple",
      difficulty: "medium",
      question:
        "In what year did &quot;The Big Bang Theory&quot; debut on CBS?",
      correct_answer: "2007",
      incorrect_answers: ["2008", "2006", "2009"],
    },
    {
      category: "Entertainment: Music",
      type: "boolean",
      difficulty: "easy",
      question:
        "The music video to The Buggle&#039;s &quot;Video Killed the Radio Star&quot; was the first music video to broadcast on MTV.",
      correct_answer: "True",
      incorrect_answers: ["False"],
    },
    {
      category: "Animals",
      type: "multiple",
      difficulty: "easy",
      question: "What is the scientific name for modern day humans?",
      correct_answer: "Homo Sapiens",
      incorrect_answers: [
        "Homo Ergaster",
        "Homo Erectus",
        "Homo Neanderthalensis",
      ],
    },
    {
      category: "Politics",
      type: "multiple",
      difficulty: "medium",
      question:
        "Who was the only president to not be in office in Washington D.C?",
      correct_answer: "George Washington",
      incorrect_answers: [
        "Abraham Lincoln",
        "Richard Nixon",
        "Thomas Jefferson",
      ],
    },
    {
      category: "Entertainment: Books",
      type: "multiple",
      difficulty: "hard",
      question:
        "In the Harry Potter universe, what is Cornelius Fudge&#039;s middle name?",
      correct_answer: "Oswald",
      incorrect_answers: ["James", "Harold", "Christopher"],
    },
    {
      category: "History",
      type: "boolean",
      difficulty: "medium",
      question:
        "In World War II, Hawker Typhoons served in the Pacific theater.",
      correct_answer: "False",
      incorrect_answers: ["True"],
    },
    {
      category: "Entertainment: Books",
      type: "multiple",
      difficulty: "medium",
      question: "By what name was the author Eric Blair better known?",
      correct_answer: "George Orwell",
      incorrect_answers: ["Aldous Huxley", "Ernest Hemingway", "Ray Bradbury"],
    },
    {
      category: "Geography",
      type: "multiple",
      difficulty: "easy",
      question: "Which Russian oblast forms a border with Poland?",
      correct_answer: "Kaliningrad",
      incorrect_answers: ["Samara", "Nizhny Novgorod", "Omsk"],
    },
    {
      category: "General Knowledge",
      type: "boolean",
      difficulty: "medium",
      question: "There are 86400 seconds in a day.",
      correct_answer: "True",
      incorrect_answers: ["False"],
    },
    {
      category: "Politics",
      type: "multiple",
      difficulty: "medium",
      question:
        "The 2014 movie &quot;The Raid 2: Berandal&quot; was mainly filmed in which Asian country?",
      correct_answer: "Indonesia",
      incorrect_answers: ["Thailand", "Brunei", "Malaysia"],
    },
    {
      category: "Science & Nature",
      type: "multiple",
      difficulty: "medium",
      question: "What is the chemical formula for ammonia?",
      correct_answer: "NH3",
      incorrect_answers: ["CO2", "NO3", "CH4"],
    },
    {
      category: "Entertainment: Video Games",
      type: "boolean",
      difficulty: "easy",
      question:
        "In Kingdom Hearts the Paopu fruit is said to intertwine the destinies for two people forever.",
      correct_answer: "True",
      incorrect_answers: ["False"],
    },
    {
      category: "Science: Mathematics",
      type: "boolean",
      difficulty: "hard",
      question:
        "If you could fold a piece of paper in half 50 times, its&#039; thickness will be 3/4th the distance from the Earth to the Sun.",
      correct_answer: "True",
      incorrect_answers: ["False"],
    },
    {
      category: "Science & Nature",
      type: "multiple",
      difficulty: "medium",
      question:
        "What does the yellow diamond on the NFPA 704 fire diamond represent?",
      correct_answer: "Reactivity",
      incorrect_answers: ["Health", "Flammability", "Radioactivity"],
    },
    {
      category: "History",
      type: "multiple",
      difficulty: "medium",
      question:
        "What is the bloodiest event in United States history, in terms of casualties?",
      correct_answer: "Battle of Antietam",
      incorrect_answers: ["Pearl Harbor", "September 11th", "D-Day"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "hard",
      question: "How many total monsters appear in Monster Hunter Generations?",
      correct_answer: "105",
      incorrect_answers: ["100", "98", "73"],
    },
    {
      category: "Vehicles",
      type: "multiple",
      difficulty: "hard",
      question: "What engine is in the Lexus SC400?",
      correct_answer: "1UZ-FE",
      incorrect_answers: ["2JZ-GTE", "7M-GTE", "5M-GE"],
    },
    {
      category: "Entertainment: Music",
      type: "multiple",
      difficulty: "medium",
      question:
        "What is the name of the main character from the music video of &quot;Shelter&quot; by Porter Robinson and A-1 Studios?",
      correct_answer: "Rin",
      incorrect_answers: ["Rem", "Ren", "Ram"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "medium",
      question:
        "Which of these songs does NOT play during the Ruins segments of the 2015 game &quot;Undertale&quot;?",
      correct_answer: "Another Medium",
      incorrect_answers: ["Anticipation", "Unnecessary Tension", "Ruins"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "hard",
      question:
        "Which day in &quot;Papers, Please&quot; does the man in red appear?",
      correct_answer: "Day 23",
      incorrect_answers: ["Day 20", "Day 17", "Day 15"],
    },
    {
      category: "Vehicles",
      type: "multiple",
      difficulty: "easy",
      question:
        "Which car tire manufacturer is famous for its &quot;Eagle&quot; brand of tires, and is the official tire supplier of NASCAR?",
      correct_answer: "Goodyear",
      incorrect_answers: ["Pirelli", "Bridgestone", "Michelin"],
    },
    {
      category: "Entertainment: Music",
      type: "boolean",
      difficulty: "easy",
      question: "Stevie Wonder&#039;s real name is Stevland Hardaway Morris.",
      correct_answer: "True",
      incorrect_answers: ["False"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "medium",
      question: "In which year did the first Monster Hunter game get released?",
      correct_answer: "2004",
      incorrect_answers: ["2006", "2005", "2002"],
    },
    {
      category: "Entertainment: Board Games",
      type: "multiple",
      difficulty: "medium",
      question: "In Yu-Gi-Oh, how does a player perform an Xyz Summon?",
      correct_answer: "Overlay at least 2 Monsters of the Same Level",
      incorrect_answers: [
        "Activate a Spell and Send Monsters to the Graveyard",
        "Add the Monsters&#039; Levels Together to Match the Xyz Monster",
        "Banish A Number of Monsters From Your Hand And Deck",
      ],
    },
    {
      category: "Celebrities",
      type: "multiple",
      difficulty: "medium",
      question:
        "Which radio personality calls himself &quot;The King of All Media&quot;?",
      correct_answer: "Howard Stern",
      incorrect_answers: ["Rush Limbaugh", "Pete Tong", "Ryan Seacrest"],
    },
    {
      category: "History",
      type: "multiple",
      difficulty: "medium",
      question:
        "Which Las Vegas casino was originally constructed and operated by mobster Bugsy Siegel?",
      correct_answer: "The Flamingo",
      incorrect_answers: ["The MGM Grand", "The Sands", "The Sahara"],
    },
    {
      category: "Entertainment: Musicals & Theatres",
      type: "multiple",
      difficulty: "hard",
      question:
        "Who serves as the speaker of the prologue in Shakespeare&#039;s Romeo and Juliet?",
      correct_answer: "Chorus",
      incorrect_answers: ["Montague", "Refrain", "Capulet"],
    },
    {
      category: "Entertainment: Japanese Anime & Manga",
      type: "multiple",
      difficulty: "hard",
      question:
        "Medaka Kurokami from &quot;Medaka Box&quot; has what abnormality?",
      correct_answer: "The End",
      incorrect_answers: ["Perfection", "Sandbox", "Fairness"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "medium",
      question:
        "In the game Pok&eacute;mon Conquest, which warlord is able to bond with Zekrom and a shiny Rayquazza?",
      correct_answer: "Nobunaga",
      incorrect_answers: ["The Player", "Oichi", "Hideyoshi"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "medium",
      question:
        "Which of these blocks in &quot;Minecraft&quot; has the lowest blast resistance?",
      correct_answer: "Sand",
      incorrect_answers: ["End Stone", "Water", "Wood Planks"],
    },
    {
      category: "Entertainment: Cartoon & Animations",
      type: "multiple",
      difficulty: "easy",
      question:
        "Which of these characters from &quot;SpongeBob SquarePants&quot; is not a squid?",
      correct_answer: "Gary",
      incorrect_answers: ["Orvillie", "Squidward", "Squidette"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "hard",
      question:
        "In the &quot;Devil May Cry&quot; franchise, which game is chronologically first?",
      correct_answer: "Devil May Cry 3: Dante&#039;s Awakening ",
      incorrect_answers: [
        "Devil May Cry 4",
        "Devil May Cry",
        "Devil May Cry 2",
      ],
    },
    {
      category: "Entertainment: Board Games",
      type: "multiple",
      difficulty: "hard",
      question:
        "What was the development code name for the &quot;Weatherlight&quot; expansion for &quot;Magic: The Gathering&quot;, released in 1997?",
      correct_answer: "Mocha Latte",
      incorrect_answers: ["Decaf", "Frappuccino", "Macchiato"],
    },
    {
      category: "Entertainment: Music",
      type: "multiple",
      difficulty: "easy",
      question:
        "The band Muse released their first album, Showbiz, in what year?",
      correct_answer: "1999",
      incorrect_answers: ["1998", "2000", "2001"],
    },
    {
      category: "Geography",
      type: "multiple",
      difficulty: "medium",
      question:
        "Which English county will you find the University of East Anglia?",
      correct_answer: "Norfolk",
      incorrect_answers: ["Suffolk", "Essex", "Cambridgeshire"],
    },
    {
      category: "General Knowledge",
      type: "multiple",
      difficulty: "easy",
      question:
        "Terry Gilliam was an animator that worked with which British comedy group?",
      correct_answer: "Monty Python",
      incorrect_answers: [
        "The Goodies&lrm;",
        "The League of Gentlemen&lrm;",
        "The Penny Dreadfuls",
      ],
    },
    {
      category: "Geography",
      type: "multiple",
      difficulty: "hard",
      question: "Which of these cities is NOT in England?",
      correct_answer: "Edinburgh",
      incorrect_answers: ["Oxford", "Manchester", "Southampton"],
    },
    {
      category: "Sports",
      type: "multiple",
      difficulty: "hard",
      question: "Who is Manchester United&#039;s leading appearance maker?",
      correct_answer: "Ryan Giggs",
      incorrect_answers: ["David Beckham", "Wayne Rooney", "Eric Cantona"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "medium",
      question:
        "In Touhou 12: Undefined Fantastic Object, which of these was not a playable character?",
      correct_answer: "Izayoi Sakuya",
      incorrect_answers: ["Hakurei Reimu", "Kirisame Marisa", "Kochiya Sanae"],
    },
    {
      category: "General Knowledge",
      type: "multiple",
      difficulty: "easy",
      question: "The Canadian $1 coin is colloquially known as a what?",
      correct_answer: "Loonie",
      incorrect_answers: ["Boolie", "Foolie", "Moodie"],
    },
    {
      category: "General Knowledge",
      type: "multiple",
      difficulty: "easy",
      question:
        "The drug cartel run by Pablo Escobar originated in which South American city?",
      correct_answer: "Medell&iacute;n",
      incorrect_answers: ["Bogot&aacute;", "Quito", "Cali"],
    },
    {
      category: "Entertainment: Video Games",
      type: "boolean",
      difficulty: "easy",
      question:
        "In the video game &quot;Splatoon&quot;, the playable characters were originally going to be rabbits instead of squids.",
      correct_answer: "True",
      incorrect_answers: ["False"],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "easy",
      question:
        "What Ultimate does Makoto Naegi, protagonist of Danganronpa: Trigger Happy Havoc, have? ",
      correct_answer: "Ultimate Lucky Student",
      incorrect_answers: [
        "Ultimate Unlucky Student",
        "Ultimate Detective",
        "Ultimate Runner",
      ],
    },
    {
      category: "Entertainment: Film",
      type: "multiple",
      difficulty: "medium",
      question:
        "What were the Chilled Monkey Brains made from during Indiana Jones and the Temple of Doom?",
      correct_answer: "Custard and Raspberry Sauce",
      incorrect_answers: [
        "Strawberry Ice Cream",
        "Cherry Yogurt",
        "Raspberry Sorbet",
      ],
    },
    {
      category: "Entertainment: Video Games",
      type: "multiple",
      difficulty: "medium",
      question:
        "In &quot;Resident Evil 2&quot;, which virus was William Birkin mutated by?",
      correct_answer: "G-Virus",
      incorrect_answers: ["T-Virus", "C-Virus", "E-Virus"],
    },
  ],
};
