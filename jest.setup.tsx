import "@testing-library/jest-dom";

import fetchMock from "fetch-mock";
import { queryClient } from "tests/utils";

afterEach(() => {
  fetchMock.reset();
  window.sessionStorage.clear();
  window.localStorage.clear();
  jest.clearAllMocks();
  queryClient.clear();
});
